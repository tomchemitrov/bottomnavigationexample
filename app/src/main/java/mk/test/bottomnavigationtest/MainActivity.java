package mk.test.bottomnavigationtest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Switch;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import mk.test.bottomnavigationtest.fragment.CartFragment;
import mk.test.bottomnavigationtest.fragment.GiftFragment;
import mk.test.bottomnavigationtest.fragment.ProfileFragment;
import mk.test.bottomnavigationtest.fragment.ShopFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationView = findViewById(R.id.bottom_navigation);
        navigationView.setOnNavigationItemSelectedListener(this);
        navigationView.setSelectedItemId(R.id.shop);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment;
        FragmentTransaction ft;

        switch(menuItem.getItemId()){
            case R.id.shop:
                fragment = ShopFragment.newInstance();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container, fragment, ShopFragment.TAG);
                ft.commit();
                return true;
            case R.id.gift:
                fragment = GiftFragment.newInstance();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container,fragment,GiftFragment.TAG);
                ft.commit();
                return true;
            case R.id.cart:
                fragment = CartFragment.newInstance();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container, fragment, CartFragment.TAG);
                ft.commit();
                return true;
            case R.id.profile:
                fragment = ProfileFragment.newInstance();
                ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container, fragment, ProfileFragment.TAG);
                ft.commit();
                return true;
            }
        return false;
    }
}
