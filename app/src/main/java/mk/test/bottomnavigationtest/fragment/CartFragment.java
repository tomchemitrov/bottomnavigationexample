package mk.test.bottomnavigationtest.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mk.test.bottomnavigationtest.R;
import mk.test.bottomnavigationtest.adapter.CartAdapter;
import mk.test.bottomnavigationtest.model.CartItem;


public class CartFragment extends Fragment {
    RecyclerView recyclerView;
    CartAdapter adapter;

    List<CartItem> cartItems = new ArrayList<>();

    public static final String TAG = CartFragment.class.getSimpleName();

    public CartFragment() {
        // Required empty public constructor
    }

    public static CartFragment newInstance(){
        return new CartFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        generateItems();

        adapter = new CartAdapter(getActivity(),cartItems);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void generateItems(){
        CartItem item1 = new CartItem("Phone", "Tech", "45.88 $");
        CartItem item2 = new CartItem("Wallet", "Category", "1.2 $");
        CartItem item3 = new CartItem("Glasses","Wear","99$");
        CartItem item4 = new CartItem("Watch","Jewelery","399$");
        CartItem item5 = new CartItem("Phone","Tech","68.12$");
        CartItem item6 = new CartItem("Item","Category","Price");

        cartItems.add(item1);
        cartItems.add(item2);
        cartItems.add(item3);
        cartItems.add(item4);
        cartItems.add(item5);
        cartItems.add(item6);
        cartItems.add(item5);
        cartItems.add(item4);
        cartItems.add(item3);
        cartItems.add(item2);
    }
}