package mk.test.bottomnavigationtest.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mk.test.bottomnavigationtest.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GiftFragment extends Fragment {

    public static final String TAG = GiftFragment.class.getSimpleName();


    public GiftFragment() {
        // Required empty public constructor
    }

    public static GiftFragment newInstance(){
        return new GiftFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gift, container, false);
    }

}
