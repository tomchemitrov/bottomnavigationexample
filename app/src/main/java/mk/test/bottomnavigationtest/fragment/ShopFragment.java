package mk.test.bottomnavigationtest.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mk.test.bottomnavigationtest.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopFragment extends Fragment {

    public static final String TAG = ShopFragment.class.getSimpleName();

    public ShopFragment() {
        // Required empty public constructor
    }

    public static ShopFragment newInstance(){
        return new ShopFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shop, container, false);
    }

}
