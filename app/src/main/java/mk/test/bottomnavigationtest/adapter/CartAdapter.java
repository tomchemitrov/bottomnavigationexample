package mk.test.bottomnavigationtest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mk.test.bottomnavigationtest.R;
import mk.test.bottomnavigationtest.model.CartItem;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    List<CartItem> items;
    LayoutInflater inflater;

    public CartAdapter (Context context, List<CartItem> data){
        this.items = data;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.cart_item, parent,false);
        return new CartViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        CartItem cart = items.get(position);

        holder.title.setText(cart.getTitle());
        holder.desc.setText(cart.getCategory());
        holder.price.setText(cart.getPrice());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder{

        TextView title, desc, price;

        public CartViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.item_title);
            desc = itemView.findViewById(R.id.item_categ);
            price = itemView.findViewById(R.id.item_price);

        }
    }
}
